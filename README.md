# Репозиторий для выполнения практических заданий на курсе "Веб-разработчик. Языки JavaScript/TypeScript.

---

### Репозиторий создан для обучения на курсе профессиональной переподготовки «[Веб-разработчик. Языки JavaScript/TypeScript](http://hse.spbstu.ru/Retraining/Programs/Javascript-Developer)» от [Высшей инженерной школы](http://hse.spbstu.ru/).

### . FRNT-TS10. Работа со структурами данных на языке TypeScript
*  [FRNT-TS10. Работа со структурами данных на языке TypeScript](http://hse.spbstu.ru/Retraining/Course/FRNT-TS10-Rabota-so-strukturami-dannyx-na-yazyke-TypeScript/2582/) - курс является продолжением ["FRNT-JS10. Программирование клиентской части на JavaScript"]((http://hse.spbstu.ru/Retraining/Course/FRNT-JS10-Programmirovanie-klientskoj-chasti-na-JavaScript/2580/)). В рамках курса слушатели знакомятся с языком TypeScript, являющемся современной заменой языка JavaScript, а также изучают работу со стандартными структурами данных с использованием этого языка и использование основных алгоритмов.
